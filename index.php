<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Instagram - popular photos</title>
        <link href="https://vjs.zencdn.net/4.2/video-js.css" rel="stylesheet">
        <link href="assets/style.css" rel="stylesheet">
        <script src="https://vjs.zencdn.net/4.2/video.js"></script>
        <script src= "assets/js/angular.min.js"></script>
        <script src= "assets/js/angular-sanitize.min.js"></script>
        <script src="assets/js/jquery.min.js"></script>
    </head>
    <body data-ng-app="myApp">
        <div class="container">
            <header class="clearfix">
                <img src="assets/instagram.png" alt="Instagram logo">
                <h1>Instagram <span>popular photos</span></h1>
            </header>
            <div class="main" data-ng-controller="mediaCtrl">
                <ul class="grid">
                    <li data-ng-repeat="mediaObj in mediaObjects">
                        <div data-ng-if="mediaObj.type=='video'">
                            <video id="video-{{$index}}" class="media video-js vjs-default-skin vjs-big-play-centered" width="250" height="250" poster="{{mediaObj.poster}}" repeat-directive>
                                <source  src="{{trustSrc(mediaObj.source)}}" type="video/mp4" />
                            </video>
                        </div>

                        <div data-ng-if="mediaObj.type=='image'">
                            <img class="media" src="{{mediaObj.image}}"/>
                        </div>

                        <div class="content">
                            <div class="avatar" style="background-image: url({{mediaObj.avatar}})"></div>
                            <p>{{mediaObj.username}}</p>
                            <div class="comment">{{mediaObj.comment}}</div>
                        </div>

                    </li>
                </ul>
            </div>
        </div>
        <!-- javascript -->

        <script type="text/javascript">
            var app = angular.module('myApp', ['ngSanitize']).directive('repeatDirective', function(){
                return function(scope, element, attrs){
                    //console.log(attrs.id)
                    if(document.getElementsByName(attrs.id).length>0) {
                        videojs(document.getElementsByName(attrs.id), {"controls":true, "autoplay": false, "preload": "auto"});
                    }
                }
            });
            app.controller('mediaCtrl', function($scope, $sce, $http){
                $http.get("getSearchResult.php").success(function(response){
                    $scope.mediaObjects = response.records;
                });
                $scope.trustSrc = function(src) {
                    return $sce.trustAsResourceUrl(src);
                }
            });
            $(document).ready(function() {
            // rollover effect
                $(document).on('mouseenter','.grid li', function(){
                    var $image = $(this).find('.media');
                    var height = $image.height();
                    $image.stop().animate({ marginTop: -(height - 82) }, 1000);
                });
                $(document).on('mouseleave', '.grid li', function(){
                    var $image = $(this).find('.media');
                    var height = $image.height();
                    $image.stop().animate({ marginTop: '0px' }, 1000);
                });
                /*$('li').hover(function() {
                    var $image = $(this).find('.media');
                    var height = $image.height();
                    $image.stop().animate({ marginTop: -(height - 82) }, 1000);
                }, function() {
                    var $image = $(this).find('.media');
                    var height = $image.height();
                    $image.stop().animate({ marginTop: '0px' }, 1000);
                });*/

            });
        </script>
    </body>
</html>
