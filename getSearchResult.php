<?php
/**
 * Created by aarticianpc.
 * User: aarticianpc
 * Date: 08-05-2015
 */

require 'src/Instagram.php';
require_once 'config.php';
use MetzWeb\Instagram\Instagram;

$instagram = new Instagram($config['client_id']);
$result = $instagram->getPopularMedia();

$media_arr = [];
$i = 0;
foreach ($result->data as $media) {
    $media_arr[$i]['type'] = $media->type;
    // output media
    if ($media->type === 'video') {
        $media_arr[$i]['poster'] = $media->images->low_resolution->url;
        $media_arr[$i]['source'] = $media->videos->standard_resolution->url;
    } else {
        // image
        $media_arr[$i]['image'] = $media->images->low_resolution->url;
    }

    // create meta section
    $media_arr[$i]['avatar'] = $media->user->profile_picture;
    $media_arr[$i]['username'] = $media->user->username;
    $media_arr[$i]['comment'] = (!empty($media->caption->text)) ? $media->caption->text : '';
    $i++;
}
echo json_encode(array('records'=>$media_arr));
exit;
